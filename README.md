## Laravel Start Up

Steps to install this project

- Clone the repository
- Move to the project folder
- composer install
- npm install
- npm audit fix
- cp .env.example .env
- Update .env file with database details
- Create database db_learn_laravel
- php artisan optimize
- php artisan view:cache
- php artisan migrate
- composer dump-autoload
- php artisan db:seed --class=PermissionsSeeder
- npm run dev

# Development Plan

- Create a git branch on your name from develop branch and start working on it.
- Once you finish a feature you can push it to the remote
- I will check it and if working you can start developing another feature.
